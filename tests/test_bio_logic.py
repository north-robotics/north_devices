import unittest
from north_devices.potentiostat.bio_logic import GeneralPotentiostat, SP150


class BioLogicTest(unittest.TestCase):
    def test_generic(self):
        potentiostat = GeneralPotentiostat('KBIO_DEV_SP100', b'USB0', 'C:\\EC-Lab Development Package\\EC-Lab Development Package\\EClib64.dll')
        potentiostat.connect()
        print(potentiostat)
