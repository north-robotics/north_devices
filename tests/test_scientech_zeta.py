import unittest
import time
import math
import logging
from ftdi_serial import Serial, MockDevice
from north_utils.test import assert_float_equal
from north_devices.scales.scientech_zeta import ScientechZeta

logger = logging.getLogger(__name__)

DEVICE_SERIAL = 'FTU9RIS7'

logging.basicConfig(level=logging.DEBUG)


class ScientechZetaTest(unittest.TestCase):
    serial: Serial = None

    @classmethod
    def setUpClass(cls):
        cls.serial = Serial(device_serial=DEVICE_SERIAL, baudrate=19200)

    @classmethod
    def tearDownClass(cls):
        cls.serial.disconnect()

    def setUp(self):
        self.scale = ScientechZeta(self.serial)

    def test_clear(self):
        self.scale.clear()

    def test_tare(self):
        self.scale.tare()

    def test_weigh(self):
        print(self.scale.weigh())


class ScientechZetaMockTest(unittest.TestCase):
    serial: Serial = None

    @classmethod
    def setUpClass(cls):
        cls.serial = Serial(device=MockDevice())

    def setUp(self):
        self.serial.device.clear()
        self.serial.device.input_buffer = b'\r\n'
        self.scale = ScientechZeta(self.serial)

    def test_weigh(self):
        self.serial.device.input_buffer += b'1.0     OK\r\n'
        weight = self.scale.weigh()
        assert_float_equal(weight, 1.0)

    def test_weigh_overload(self):
        self.serial.device.input_buffer += b'OL      OK\r\n'
        weight = self.scale.weigh()
        assert math.isnan(weight)

    def test_settled_weigh(self):
        self.serial.device.input_buffer += b'OL       OK\r\n'
        self.serial.device.input_buffer += b'OL       OK\r\n'
        self.serial.device.input_buffer += b'OL       OK\r\n'
        self.serial.device.input_buffer += b'0.2      OK\r\n'
        self.serial.device.input_buffer += b'0.3      OK\r\n'
        self.serial.device.input_buffer += b'0.4      OK\r\n'
        self.serial.device.input_buffer += b'0.5      OK\r\n'
        self.serial.device.input_buffer += b'0.5      OK\r\n'
        self.serial.device.input_buffer += b'0.5      OK\r\n'

        weight = self.scale.settled_weigh()
        assert_float_equal(weight, 0.5)